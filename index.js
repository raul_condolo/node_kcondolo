const express = require('express');
require('dotenv').config();
const cors = require('cors');
//crear servidor
const app = express();
const whitelist = ["http://localhost:3000"]
const corsOptions = {
    origin: function (origin, callback) {
        if (!origin || whitelist.indexOf(origin) !== -1) {
            callback(null, true)
        } else {
            callback(new Error("Not allowed by CORS"))
        }
    },
    credentials: true,
}
app.use(cors(corsOptions))
//Directorio público
app.use(express.static('public'));

app.use(express.json());

//Rutas
//autenticación
app.use('/api/auth', require('./routes/auth'));
//eventos de administración
app.use('/api/sorteos', require('./routes/sorteos'));
//eventos de sistemas
app.use('/api/sistemas', require('./routes/sistemas'))
//Escuchar peticiones
app.listen(process.env.PORT, () => {
    console.log(`Servidor corriendo en puerto ${process.env.PORT}`);
})