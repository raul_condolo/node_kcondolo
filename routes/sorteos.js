const { Router } = require('express');
const { getGanadoresVigentes, comprobarGanadores } = require('../controllers/sorteos')
const router = Router();
require('dotenv').config();


//ruta 1
router.get('/getGanadoresVigentes', getGanadoresVigentes)
router.post('/comprobarGanadores', comprobarGanadores)

module.exports = router;