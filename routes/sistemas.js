const { Router } = require('express');
const { getInfoSistema, getArchivo, llenarExcel } = require('../controllers/sistemas')
const router = Router();
require('dotenv').config();

// ruta1
router.get('/getInfoSistema', getInfoSistema)
//ruta2
router.post('/getArchivo', getArchivo)
router.post('/llenarExcel', llenarExcel)
module.exports = router;