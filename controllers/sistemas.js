const { Workbook } = require('excel4node');
const { response } = require('express')
const bd = 'meg_proveeduria'
const fs = require('fs');
const path = require('path');
const getInfoSistema = (req, res = response) => {
    const nombre_sistema = process.env.NOMBRE_SISTEMA
    const valor = -1;
    var Sybase = require("../src/SybaseDB");
    db = new Sybase(process.env.HOST, process.env.PORT_DB, bd, process.env.USER, process.env.PW);
    db.connect(function (err) {
        if (err) return console.log(err);
        db.query(`exec stp_getCuantoCuando3 ${valor}`, function (err, data) {
            if (err) console.log(err);
            db.disconnect();
            res.json({
                data: data
            })
            console.log(data)
        });

    });

}

const getArchivo = (req, res = response) => {
    const Excel = require('exceljs');
    const express = require('express');
    const { dirname } = require('path');
    const app = express();

    // Crear una nueva instancia de Excel
    const workbook = new Excel.Workbook();

    // Agregar una hoja de cálculo
    const sheet = workbook.addWorksheet('Datos');

    // Agregar una imagen al archivo
    const logo = workbook.addImage({
        buffer: fs.readFileSync(__dirname + '/' + 'Imagen1.jpg')
    });

    // Especificar la posición de la celda donde se insertará la imagen
    sheet.addImage(logo, {
        tl: { col: 1, row: 1 },
        br: { col: 2, row: 6 },
    });
    // Definir el ancho de las columnas
    sheet.getColumn('A').width = 20;
    sheet.getColumn('B').width = 10;
    sheet.getColumn('C').width = 15;

    // Definir el tipo de fuente para la hoja de cálculo
    sheet.getCell('A1').font = { name: 'Arial', size: 14, bold: true };
    sheet.getCell('B1').font = { name: 'Arial', size: 14, bold: true };
    sheet.getCell('C1').font = { name: 'Arial', size: 14, bold: true };

    // Escribir datos en la hoja de cálculo
    sheet.columns = [
        { header: 'Nombre', key: 'nombre' },
        { header: 'Edad', key: 'edad' },
        { header: 'Ciudad', key: 'ciudad' },
    ];

    sheet.addRows([
        { nombre: 'Juan', edad: 32, ciudad: 'Madrid' },
        { nombre: 'María', edad: 28, ciudad: 'Barcelona' },
        { nombre: 'Pedro', edad: 41, ciudad: 'Valencia' },
    ]);
    const nombre_archivo = 'BD_SCRIPTS_NODE.xlsx'
    const desktopPath = process.env.USERPROFILE || process.env.HOME;
    const filePath = path.join(desktopPath, 'Desktop', nombre_archivo);
    console.log(filePath)
    // Guardar el archivo de Excel
    workbook.xlsx.writeFile(filePath, res)
        .then(() => {
            console.log('Archivo guardado');
            res.json({
                respuesta: 200,
                mensaje: 'El archivo se guardó correctamente en tu escritorio',
                archivo: nombre_archivo
            })
            res.end();
        });
};

const llenarExcel = async (req, res = response) => {

    const today = new Date();
    const day = today.getDate();
    const month = today.getMonth() + 1;
    const year = today.getFullYear();
    const { persona_solicita, persona_realiza, justificacion, detalle_script,contenido_sql } = req.body;
    const Excel = require('exceljs');
    const workbook = new Excel.Workbook();
    await workbook.xlsx.readFile(__dirname + '/' + 'BDScript.xlsx');
    const worksheet = workbook.getWorksheet(1);

    worksheet.getCell('Z9').value = day;
    worksheet.getCell('AB9').value = month;
    worksheet.getCell('AD9').value = year;
    worksheet.getCell('I12').value = day + '/' + month + '/' + year;
    worksheet.getCell('B19').value = persona_solicita;
    worksheet.getCell('M16').value = justificacion;
    worksheet.getCell('B22').value = detalle_script;
    worksheet.getCell('C27').value = contenido_sql;
    worksheet.getCell('Z46').value = persona_realiza;
    
    const nombre_archivo = 'BD_SCRIPTS_NODE_LLENADO.xlsx'
    const desktopPath = process.env.USERPROFILE || process.env.HOME;
    const filePath = path.join(desktopPath, 'Desktop', nombre_archivo);
    workbook.xlsx.writeFile(filePath, res)
        .then(() => {
            console.log('Archivo guardado');
            res.json({
                respuesta: 200,
                mensaje: 'El archivo se guardó correctamente en tu escritorio',
                archivo: nombre_archivo
            })
            res.end();
        });
}



module.exports = {
    getInfoSistema,
    getArchivo,
    llenarExcel
}