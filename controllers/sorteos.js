const { response } = require('express')

const getGanadoresVigentes = (req, res = response) => {

    var Sybase = require("../src/SybaseDB");
    db = new Sybase(process.env.HOST, process.env.PORT_DB, process.env.DB, process.env.USER, process.env.PW);

    db.connect(function (err) {
        if (err) return console.log(err);
        db.query(`exec get_ganadores_vigentes`, function (err, data) {
            if (err) console.log(err);
            db.disconnect();
            res.json({
                data: data
            })
        });
    });
}


const comprobarGanadores = (req, res = response) => {
    var expect = require("chai").expect;
    var Sybase = require("../src/SybaseDB");
    var P = require("bluebird");
    const { tipo_consumo } = req.body;
    var host = process.env.HOST_DB,
        port = 7000,
        user = 'kcondolo',
        pw = 'kcondolo.0325',
        db = "meg_sorteos";

    db = new Sybase(host, port, db, user, pw);

    db.connect(function (err) {
        if (err) return console.log(err);

        db.query(`declare @o_error  int


        select @o_error = NULL
        
        execute dbo.get_comprobar_ganadores ${tipo_consumo}, @o_error out`, function (err, data) {

            if (err) console.log(err);
            db.disconnect();
            res.json({
                data: data
            })
        });

    });
}

module.exports = {
    getGanadoresVigentes,
    comprobarGanadores
}