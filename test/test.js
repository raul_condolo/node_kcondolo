
var expect = require("chai").expect;
var Sybase = require("../src/SybaseDB.js");
var P = require("bluebird");

//Configure To Connect To Your database here:
var host = '192.168.55.28',
	port = 7000,
	user = 'kcondolo',
	pw = 'kcondolo.0325',
	db = "meg_sorteos";
//
describe("Sorteos", function () {

	var subject;
	var connectError;

	before(function (done) {
		subject = new Sybase(host, port, db, user, pw, true);
		subject.connect(function (err) {
			connectError = err;
			done();
		});
	});

	after(function (done) {
		subject.disconnect();
		done();
	});

	it("Connect", function (done) {
		expect(connectError).to.equal(null);
		expect(subject.isConnected()).to.equal(true);
		done();
	});

	it("Consulta simple", function (done) {

		if (!subject.isConnected()) {
			expect(connectError).to.equal(null);
			done();
			return;
		}

		subject.query("exec get_ganadores_vigentes", function (err, data) {

			expect(err).to.equal(null);

			expect(data).to.be.a('array');
			console.log(data);
			done();
		});

	});

});